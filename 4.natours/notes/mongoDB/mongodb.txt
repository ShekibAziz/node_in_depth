// mongod --dbpath ~/data/db Starting mongo on localhost

// To connect to the remote type in the shell =>  mongo "mongodb+srv://cluster0-9t9uz.mongodb.net/test"  --username shekib

1) Each  DB has collections inside it and collections have documents in it. // DB => COllection => Documents.

2) use {db-name}  => switches into that database (if a db with that name doesn't exist it creates one a switches into it')

3) show dbs => shows all databases

4) show collections => shows all collections
---------------------------------

3) db.tours.insertOne(
  { 
    name: "The forest Hiker", 
    price: 297
  }
  ) => inserts a document into the current database wit the collection name tours.

  -----------

  db.tours.insertMany([
    { 
      name: "The forest Hiker", 
      price: 297
    },
    { 
      name: "The One", 
      price: 497,
      difficulty: "easy"
    }
  ]) => inserts many documents into the current database with the collection name tours.

4) db.tours.find() => returns all the documents

  or you could pass a filter object into the parentesis of find()

  db.tours.find({price: {$lte:500}}) => returns all items prices at less then 500 (inclusive of 500) 
  db.tours.find({price: {$lte:500}, rating: {$gt:4.7}}) => returns all items prices at less then 500 (inclusive of 500)  AND rating greater than 4.7
  db.tours.find({$or : [{price: {$lte:500}}, {rating: {$gt:4.7}}] }) => returns all items prices at less then 500 (inclusive of 500)  OR rating greater than 4.7

  db.tours.find({price: {$lte: 500}}, {name:1}) => return all items at less then 501 and only shows the name field.
  // send object is to specify the field we want to be shown

5) db.tours.updateOne({name: "The Snow Adventure"}, {$set:{price: 597}}) 
  => first object specifies what we want, the second one what we want to change (will only update the first one because it says update one)
   db.tours.updateMany({name: "The Snow Adventure"}, {$set:{price: 597}}) 
  => same as the operation above but this one will update all the matching Ones instead of just the first one

6) db.tours.replaceOne and db.tours.replaceMany => ({}, {})
// what we want to match and what we want it to have.
{"The Snow Adventurer", "price" : 597, "rating" : 4.9, "difficulty" : "easy"}
// db.tours.replaceOne({name: "The Snow Adventurer"}, {price: 597})
{"price" : 597}

7) db.tours.deleteMany({ rating: {$lte : 4.7}}) => delete a documents with the creatiria in the {}.
db.tours.deleteMany({}) // delete all the documents in a collection
-----------------------
TODO: KEYWORDS

$lte
$lt
$gte
$gt
$or