const fs = require('fs');
const superagent = require('superagent');

const readFilePro = (file) => {
	return new Promise((resolve, reject) => {
		fs.readFile(file, (err, data) => {
			if (err) return reject(err);
			resolve(data);
		});
	});
};

const writeFilePro = (file, data) => {
	return new Promise((resolve, reject) => {
		fs.writeFile(file, data, (err) => {
			if (err) return reject(err);
			resolve('success');
		});
	});
};

// readFilePro(`${__dirname}/dog.txt`)
// 	.then((data) => {
// 		console.log('Breed: ', data.toString());
// 		return superagent.get(`https://dog.ceo/api/breed/${data.toString()}/images/random`);
// 	})
// 	.then((res) => {
// 		console.log(res.body.message);
// 		return writeFilePro('dog-img.txt', res.body.message);
// 	})
// 	.then(() => console.log('Random dog image saved to file'))
// 	.catch((err) => console.log('ERROR', err.message));

const getDogPic = async () => {
	try {
		const data = await readFilePro(`${__dirname}/dog.txt`);
		console.log('Breed: ', data.toString());

		const res = await superagent.get(`https://dog.ceo/api/breed/${data.toString()}/images/random`);
		console.log(res.body.message);

		await writeFilePro('dog-img.txt', res.body.message);
		console.log('Random dog image saved to file');
	} catch (err) {
		console.log(err);
		throw err;
	}
	return '2';
};

// console.log('1');
// console.log(getDogPic());
// console.log('3');

// console.log('1');
// getDogPic()
// 	.then((x) => {
// 		console.log(x);
// 		console.log('3');
// 	})
// 	.catch((err) => console.log(err));

(async () => {
	try {
		console.log('1');
		const x = await getDogPic();
		console.log(x);
		console.log('3');
	} catch (err) {
		console.log(err);
	}
})();

// //////////////////////////////// // // /// ///////////
// Waiting for multiple api calls to return. Ex.  what if we need 3 dog pictures.
// we could await all of them but it would be  inefficient because we dont need to wait for the first picture to load for us to send a request
// to the second one. instead we could send all the request and then await till all three load .

const getDogPic2 = async () => {
	try {
		const data = await readFilePro(`${__dirname}/dog.txt`);
		console.log('Breed: ', data.toString());

		const res1Pro = await superagent.get(`https://dog.ceo/api/breed/${data.toString()}/images/random`);
		const res2Pro = await superagent.get(`https://dog.ceo/api/breed/${data.toString()}/images/random`);
		const res3Pro = await superagent.get(`https://dog.ceo/api/breed/${data.toString()}/images/random`);
		const all = await Promise.all([ res1Pro, res2Pro, res3Pro ]);
		const imgs = all.map((el) => el.body.message);
		console.log(imgs);

		await writeFilePro('dog-img.txt', imgs.join('\n'));
		console.log('Random dog image saved to file');
	} catch (err) {
		console.log(err);
		throw err;
	}
	return '2';
};

(async () => {
	try {
		const x = await getDogPic2();
		console.log(x);
	} catch (err) {
		console.log(err);
	}
})();
