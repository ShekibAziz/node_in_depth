// "dependencies" : {
//   'react': "^1.20.3"
// }

// 1 => first number stand for major release. Meaning it could potentially break the code. Also usually means a lot of changes.
// 20 => (minor releases) stand for improvment or additional functionality nothing breaking.
// 3 => (patches) bug fixes
// ^ stand for allowing to update the package's smaller updates which is the 20 and buf fixes which is the 3
// ~ only patch releases (safest)
// * all versions most up to date

// npm outdated => show all the packages that are outdated
// npm install react@2.0.0 => specific version of a package
// npm install => downloads all the packages
// npm update (name of the package)
// npm uninstall (name of the package)
