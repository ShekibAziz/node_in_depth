// to enter the node repl mode // node and hit enter
// to exit the node repl mode // .exit

// See all available node build in npm modules
//1) Type node in the terminal hit enter
//2) Press tab a few times and all available modules that are for use will be shown

// _ is the previous values
// Ex: 5+5 // 10
//     _+5 // 15

// Node has a built in auto complete
// Ex. string. // hit tab and will show you all available methods
