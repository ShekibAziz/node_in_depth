// Node.js process => Single Thread // where our code is executed. Only one thread
// That means that all users who are using our application are using the same thread
// Non-blocking I/O model (I/O => input/output => accessing file system => handling network requests )

//PITFALLS
// Callbacks != Asynchronous
