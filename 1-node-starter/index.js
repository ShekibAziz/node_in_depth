const fs = require('fs');
const http = require('http');
const url = require('url');
const slugify = require('slugify');
const replaceTemplate = require('./modules/replaceTemplate');

// //////////////////////////////////
// FILES

// // Blocking, synchronous way
// const textIn = fs.readFileSync('./txt/input.txt', 'utf-8');
// console.log(textIn);
// const textOut = `This is what we know about the avocado: ${textIn}.\nCreated on ${Date.now()}`;
// fs.writeFileSync('./txt/output.txt', textOut);
// console.log('File written!');

// // Non-blocking, synchronous way
// fs.readFile('./txt/start.txt', 'utf-8', (err, data1) => {
// 	if (err) return console.log('-- ERROR');
// 	fs.readFile(`./txt/${data1}.txt`, 'utf-8', (err, data2) => {
// 		console.log('data2 : ', data2);
// 		fs.readFile('./txt/append.txt', 'utf-8', (err, data3) => {
// 			console.log('data3: ', data3);

// 			fs.writeFile('./txt/final.txt', `${data2}\n${data3}`, 'utf-8', (err) => {
// 				console.log('You file bas been written ');
// 			});
// 		});
// 	});
// 	console.log('data1: ', data1);
// });
// console.log('Will read file!');

// //////////////////////////////////
// Server

// we put this here because this will only be ran once instead of every time by every user.
// This is wont be blocking the thread. If we would have put it in the
//  server then that would have been executed every time a person htis our api but here it executes only ones
const tempOverview = fs.readFileSync(`${__dirname}/templates/template-overview.html`, 'utf-8');
const tempCard = fs.readFileSync(`${__dirname}/templates/template-card.html`, 'utf-8');
const tempProduct = fs.readFileSync(`${__dirname}/templates/template-product.html`, 'utf-8');

const data = fs.readFileSync(`${__dirname}/dev-data/data.json`, 'utf-8');
const dataObj = JSON.parse(data);

const slugs = dataObj.map((element) => slugify(element.productName), { lower: true });

const server = http.createServer((req, res) => {
	const { query, pathname } = url.parse(req.url, true);

	// Overview page
	if (pathname === '/' || pathname === '/overview') {
		res.writeHead(200, { 'Content-type': 'text/html' });

		const cardsHtml = dataObj.map((element) => replaceTemplate(tempCard, element)).join('');
		const output = tempOverview.replace('{%PRODUCT_CARDS%}', cardsHtml);
		res.end(output);

		// Product page
	} else if (pathname === '/product') {
		res.writeHead(200, { 'Content-type': 'text/html' });
		const product = dataObj[query.id];
		const output = replaceTemplate(tempProduct, product);

		res.end(output);
		// API
	} else if (pathname === '/api') {
		res.writeHead(200, { 'Content-type': 'Application/json' });
		res.end(data);

		// NOT FOUND
	} else {
		res.writeHead(404, {
			'Content-type': 'text/html',
			'my-own-header': 'hello-world'
		});
		res.end('<h1>Page not found!</h1>');
	}
});

server.listen(8000, '127.0.0.1', () => {
	console.log('Listening on port 8000');
});
